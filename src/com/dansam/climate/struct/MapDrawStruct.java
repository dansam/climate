package com.dansam.climate.struct;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.Marker;

/**
 * A struct that contains the last circle and marker that
 * were drawn on the map
 * @author samkirton
 */
public class MapDrawStruct {
	private Bitmap mIcon;
	private Marker mMarker;
	
	public Bitmap getIcon() {
		return mIcon;
	}
	
	public void setIcon(Bitmap newVal) {
		mIcon = newVal;
	}
	
	public Marker getMarker() {
		return mMarker;
	}
	
	public void setMarker(Marker newVal) {
		mMarker = newVal;
	}
}