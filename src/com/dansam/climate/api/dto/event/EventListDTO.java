// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dansam.climate.api.dto.base.IDTO;

public final class EventListDTO implements IDTO {
	private EventDTO[] events;

	public EventDTO[] getEvents() { 
		return events;
	}

	public void setEvents(EventDTO[] newVal) { 
		events = newVal;
	}

	public EventListDTO() { }

	public EventListDTO(JSONObject jsonObject) throws JSONException {
		fromJson(jsonObject);
	}

	@Override
	public void fromJson(JSONObject jsonObject) throws JSONException {

		this.events = null;
		if (jsonObject.has("events")) {
			JSONArray jsonArrayEvents = new JSONArray(jsonObject.getString("events"));
			this.events = new EventDTO[jsonArrayEvents.length()];
			for (int i = 0; i < jsonArrayEvents.length(); i++) {
				EventDTO eventsToValue = new EventDTO(jsonArrayEvents.getJSONObject(i));
				this.events[i] = eventsToValue;
			}
		}
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONArray jsonArrayEvents = new JSONArray();
		if (this.events instanceof EventDTO[]) {
			for (int i = 0; i < this.events.length; i++) {
				JSONObject jsonObjectEvents = this.events[i].toJson();
				jsonArrayEvents.put(jsonObjectEvents);
			}
			jsonObject.put("events",jsonArrayEvents);
		}

		return jsonObject;
	}
}