// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

public class FloodStatusENUM {
	public static final String FLOOD_ALERT = "FLOOD_ALERT";
	public static final String WARNING = "WARNING";
	public static final String SEVERE = "SEVERE";

	private String value;

	public FloodStatusENUM(String value) {
		if (value.equals(FLOOD_ALERT) || value.equals(WARNING) || value.equals(SEVERE)) {
			this.value = value;
		} else {
			throw new IllegalArgumentException("Invalid enum value");
		}
	}

	public String getValue() {
		return value;
	}
}