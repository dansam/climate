package com.dansam.climate.fragment;

import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.dansam.climate.api.dto.base.IDTO;
import com.dansam.climate.api.dto.common.ErrorDTO;
import com.dansam.climate.api.dto.event.EventDTO;
import com.dansam.climate.api.dto.event.EventTypeENUM;
import com.dansam.climate.api.dto.event.FloodStatusENUM;
import com.dansam.climate.api.dto.event.LatlngDTO;
import com.dansam.climate.api.dto.event.StatusENUM;
import com.dansam.climate.volley.provider.VolleyRequest;
import com.dansam.climate.volley.provider.VolleyRequest.VolleyResponseCallback;
import com.dansam.climate.volley.provider.VolleyRequestProvider;
import com.pingoverthere.R;

/**
 * Posts an event to the web service
 * @author samkirton
 */
public class SaveEventFragment extends DialogFragment implements OnClickListener, VolleyResponseCallback {
	private EditText uiEnterAMessageEditText;
	private Button uiSendPingButton;
	private ProgressBar uiProgressBar;
	
	private String mLongitude;
	private String mLatitude;
	private VolleyRequestProvider mVolleyRequestProvider;
	private VolleyRequest mSaveEventVolleyRequest;
	
	public static final String EXTRA_LONGITUDE = "EXTRA_LONGITUDE";
	public static final String EXTRA_LATITUDE = "EXTRA_LATITUDE";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NORMAL, R.style.Dialog);
		
		mLongitude = getArguments().getString(EXTRA_LONGITUDE);
		mLatitude = getArguments().getString(EXTRA_LATITUDE);
		
		mVolleyRequestProvider = new VolleyRequestProvider(getActivity());
		mVolleyRequestProvider.setVolleyResponseCallback(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_save_event, container, false);
		uiEnterAMessageEditText = (EditText)view.findViewById(R.id.fragment_save_event_message);
		uiSendPingButton = (Button)view.findViewById(R.id.fragment_save_event_send_ping);
		uiProgressBar = (ProgressBar)view.findViewById(R.id.fragment_save_event_loading);
		
		uiSendPingButton.setOnClickListener(this);
		return view;
	}
	
	@Override
	public void onClick(View v) {
		if (v == uiSendPingButton) {
			uiSendPingButton.setVisibility(View.GONE);
			uiProgressBar.setVisibility(View.VISIBLE);
			
			LatlngDTO latlngDTO = new LatlngDTO();
			latlngDTO.setLatitude(mLatitude);
			latlngDTO.setLongitude(mLongitude);
			
			EventDTO eventDTO = new EventDTO();
			eventDTO.setEventType(new EventTypeENUM(EventTypeENUM.IWANT));
			eventDTO.setFloodStatus(new FloodStatusENUM(FloodStatusENUM.FLOOD_ALERT));
			eventDTO.setLatlng(latlngDTO);
			eventDTO.setMessage(uiEnterAMessageEditText.getText().toString());
			eventDTO.setMobileNumber("+447735486344");
			eventDTO.setStatus(new StatusENUM(StatusENUM.ACTIVE));
			eventDTO.setTimestamp(System.currentTimeMillis());
			
			mSaveEventVolleyRequest = mVolleyRequestProvider.saveEvent(eventDTO, null);
		}
	}

	@Override
	public void onVolleyResponse(String url, IDTO dto, Map<String, String> headers) {
		if (url.equals(mSaveEventVolleyRequest.getUrl())) {
			dismiss();
		}
	}

	@Override
	public void onVolleyError(String url, ErrorDTO errorDTO) {
		if (url.equals(mSaveEventVolleyRequest.getUrl())) {
			uiSendPingButton.setVisibility(View.VISIBLE);
			uiProgressBar.setVisibility(View.GONE);
		}
	}
}
