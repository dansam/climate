package com.dansam.climate.asynctask;

import android.content.Context;

import com.dansam.climate.asynctask.base.BaseAsyncTask;
import com.dansam.climate.asynctask.base.BaseResponse;
import com.dansam.climate.asynctask.base.IParam;
import com.dansam.climate.asynctask.param.TestParam;
import com.dansam.climate.asynctask.response.TestResponse;

/**
 * Run the code in the run() method in a seperate thread and return the result to the ui
 * via the AsyncCallback interface of the BaseAsyncTask class
 * @author samkirton
 */
public class TestAsyncTask extends BaseAsyncTask {

	public TestAsyncTask(Context context) {
		super(context);
	}

	@Override
	protected BaseResponse run(IParam param) {
		TestParam testParam = (TestParam)param;
		TestResponse testResponse = new TestResponse();
		
		// do something with the parameter provided in a seperate thread
		String testVal = testParam.getTestVal();
		
		// send a response to the activity from this thread, the AsyncCallback onAsyncTaskFinished() method
		String responseVal = "This is a response and the param was: " + testVal;
		testResponse.setResponseVal(responseVal);
		
		return testResponse;
	}
}