package com.dansam.climate;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.sqlite.SQLProvider;
import com.dansam.climate.sql.SQLInitProvider;

/**
 * A singleton that should contain application data that is common across
 * multiple activities. E.g: SQLProvider
 * @author samkirton
 */
public class ClimateApplication extends Application {
	private static ClimateApplication mInstance;
	private RequestQueue mRequestQueue;
	private SQLProvider mSQLProvider;

	public static ClimateApplication getInstance() {
		return mInstance;
	}
	
	public RequestQueue getRequestQueue() {
		return mRequestQueue;
	}
	
	public SQLProvider getSQLProvider() {
		return mSQLProvider;
	}
	
	@Override
	public final void onCreate() {
		super.onCreate();
		mInstance = this;

		mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		
		SQLInitProvider sqlInitProvider = new SQLInitProvider(getBaseContext());
		mSQLProvider = new SQLProvider(sqlInitProvider.getWritableDatabase());
	}
}
