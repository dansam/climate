package com.dansam.climate.sql.model;

import com.app.sqlite.base.BaseModel;

import java.util.HashMap;

import android.content.ContentValues;

public class EventCacheModel extends BaseModel {
	private int pid;
	private String eventType;
	private String status;
	private String floodStatus;
	private String latitude;
	private String longitude;
	private String mobileNumber;
	private long timestamp;

	private static final String COLUMN_PID = "pid";
	private static final String COLUMN_EVENTTYPE = "eventType";
	private static final String COLUMN_STATUS = "status";
	private static final String COLUMN_FLOODSTATUS = "floodStatus";
	private static final String COLUMN_LATITUDE = "latitude";
	private static final String COLUMN_LONGITUDE = "longitude";
	private static final String COLUMN_MOBILENUMBER = "mobileNumber";
	private static final String COLUMN_TIMESTAMP = "timestamp";

	// The SQL provider uses reflection to retrieve the table name from this variable
	public static final String TABLE_NAME = "EventCache";

	public int getPid() {
		return pid;
	}

	public void setPid(int newVal) {
		pid = newVal;
	}
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String newVal) {
		eventType = newVal;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String newVal) {
		status = newVal;
	}
	public String getFloodStatus() {
		return floodStatus;
	}

	public void setFloodStatus(String newVal) {
		floodStatus = newVal;
	}
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String newVal) {
		latitude = newVal;
	}
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String newVal) {
		longitude = newVal;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String newVal) {
		mobileNumber = newVal;
	}
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long newVal) {
		timestamp = newVal;
	}

	@Override
	public HashMap<String,Integer> getModelColumnTypeMap() {
		HashMap<String,Integer> modelColumns = new HashMap<String,Integer>();
		modelColumns.put(COLUMN_PID, BaseModel.FIELD_INTEGER);
		modelColumns.put(COLUMN_EVENTTYPE, BaseModel.FIELD_STRING);
		modelColumns.put(COLUMN_STATUS, BaseModel.FIELD_STRING);
		modelColumns.put(COLUMN_FLOODSTATUS, BaseModel.FIELD_STRING);
		modelColumns.put(COLUMN_LATITUDE, BaseModel.FIELD_STRING);
		modelColumns.put(COLUMN_LONGITUDE, BaseModel.FIELD_STRING);
		modelColumns.put(COLUMN_MOBILENUMBER, BaseModel.FIELD_STRING);
		modelColumns.put(COLUMN_TIMESTAMP, BaseModel.FIELD_LONG);
		return modelColumns;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues contentValues = new ContentValues();
		contentValues.put(COLUMN_EVENTTYPE, getEventType());
		contentValues.put(COLUMN_STATUS, getStatus());
		contentValues.put(COLUMN_FLOODSTATUS, getFloodStatus());
		contentValues.put(COLUMN_LATITUDE, getLatitude());
		contentValues.put(COLUMN_LONGITUDE, getLongitude());
		contentValues.put(COLUMN_MOBILENUMBER, getMobileNumber());
		contentValues.put(COLUMN_TIMESTAMP, getTimestamp());
		return contentValues;
	}
}