// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

public class EventTypeENUM {
	public static final String ROAD = "ROAD";
	public static final String CLEAR = "CLEAR";
	public static final String FIRE = "FIRE";
	public static final String IHAVE = "IHAVE";
	public static final String IWANT = "IWANT";
	public static final String IMHERE = "IMHERE";

	private String value;

	public EventTypeENUM(String value) {
		if (value.equals(ROAD) || value.equals(CLEAR) || value.equals(FIRE) || value.equals(IHAVE) || value.equals(IWANT) || value.equals(IMHERE)) {
			this.value = value;
		} else {
			throw new IllegalArgumentException("Invalid enum value");
		}
	}

	public String getValue() {
		return value;
	}
}