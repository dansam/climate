package com.dansam.climate.activity;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.FrameLayout;

import com.dansam.climate.activity.base.BaseActivity;
import com.dansam.climate.api.dto.base.IDTO;
import com.dansam.climate.api.dto.common.ErrorDTO;
import com.dansam.climate.api.dto.event.EventDTO;
import com.dansam.climate.api.dto.event.EventListDTO;
import com.dansam.climate.fragment.BoundaryMapFragment;
import com.dansam.climate.fragment.ModalDialogFragment;
import com.dansam.climate.volley.provider.VolleyRequest;
import com.dansam.climate.volley.provider.VolleyRequest.VolleyResponseCallback;
import com.dansam.climate.volley.provider.VolleyRequestProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pingoverthere.R;

/**
 * The main activity of the application
 * @author samkirton
 *
 */
public class MainActivity extends BaseActivity implements VolleyResponseCallback {
	private BoundaryMapFragment uiMapFragment;
	
	private VolleyRequestProvider mVolleyRequestProvider;
	private VolleyRequest mGetEventsVolleyRequest;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		super.useNavigationDrawer();
		
		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
			System.out.println("Service available");
		} else {
			System.out.println("Service not available");
		}
		
		uiMapFragment = (BoundaryMapFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_boundary_map);
		
		mVolleyRequestProvider = new VolleyRequestProvider(this);
		mVolleyRequestProvider.setVolleyResponseCallback(this);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		mGetEventsVolleyRequest = mVolleyRequestProvider.getEvent(100, 100, 10, null);
	}
	
	@Override
	protected View getContentLayout() {
		// TODO Auto-generated method stub
		return findViewById(R.id.activity_main_content_layout);
	}

	@Override
	protected FrameLayout getNavigationContentLayout() {
		// TODO Auto-generated method stub
		return (FrameLayout)findViewById(R.id.activity_main_navigation_fragment);
	}

	@Override
	protected DrawerLayout getNavigationDrawer() {
		// TODO Auto-generated method stub
		return (DrawerLayout)findViewById(R.id.main_activity_navigation_drawer);
	}

	@Override
	public void onVolleyResponse(String url, IDTO dto, Map<String, String> headers) {
		if (url.equals(mGetEventsVolleyRequest.getUrl())) {
			EventListDTO eventListDTO = (EventListDTO)dto;
			if (eventListDTO.getEvents() != null && eventListDTO.getEvents().length > 0) {
				uiMapFragment.init(eventListDTO.getEvents());
			}
		}
	}

	@Override
	public void onVolleyError(String url, ErrorDTO errorDTO) {
		if (url.equals(mGetEventsVolleyRequest.getUrl())) {
			showModalDialog("Could not download pins, your existing pins have been displayed.",ModalDialogFragment.BUTTON_TYPE_OK);
		}
	}
}
