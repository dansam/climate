package com.dansam.climate.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

/**
 * 
 * @author samkirton
 */
public class GPSLocationProvider extends Service implements LocationListener {
	private Context mContext;
	private double latitude; 
	private double longitude;
	private LocationCallback mLocationCallback;
	
	private static final long CHANGE_DISTANCE_METERS_UPDATE = 10;
	private static final long TIME_DURATION_UPDATE = 300000; // 5 minutes
	
	public interface LocationCallback {
		public void onLocationRetrieved(Location location);
		public void onProviderDisabled();
	}
	
	public void setLocationCallback(LocationCallback locationCallback) {
		mLocationCallback = locationCallback;
	}
	
	public GPSLocationProvider(Context context) {
		mContext = context;
	}
	
	public Location getLocation() throws GPSConnectivityException {
		Location location = null; 
		LocationManager locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

		boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		
		if (!isGPSEnabled && !isNetworkEnabled) {
			throw new GPSConnectivityException();
		} else {
			// prefer network over GPS		
			if (isNetworkEnabled) {
				locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER,
					TIME_DURATION_UPDATE,
					CHANGE_DISTANCE_METERS_UPDATE, 
					this
				);
				
				if (locationManager != null) {
					location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					if (location != null) {
						latitude = location.getLatitude();
						longitude = location.getLongitude();
					}
				}
			}
			
			// if network is not available, use the GPS
			if (isGPSEnabled && location == null) {
				locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					TIME_DURATION_UPDATE,
					CHANGE_DISTANCE_METERS_UPDATE, 
					this
				);
				
				if (locationManager != null) {
					location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					if (location != null) {
						latitude = location.getLatitude();
						longitude = location.getLongitude();
					}
				}
			}
		}
		
		return location;
	}
	
	@Override
	public void onLocationChanged(Location location) {
		mLocationCallback.onLocationRetrieved(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
		mLocationCallback.onProviderDisabled();
	}

	@Override
	public void onProviderEnabled(String provider) {
		Location location = null;
		
		try {
			location = getLocation();
		} catch (GPSConnectivityException e) { }
		this.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}