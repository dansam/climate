package com.dansam.climate.helper;

import java.util.HashMap;

public class RESTHelper {

	/**
	 * @return	The default REST headers
	 */
	public static HashMap<String,String> getDefaultHeadersList() {
		HashMap<String,String> defaultHeaders = new HashMap<String,String>();
		defaultHeaders.put("Accept", "application/json");
		defaultHeaders.put("User-Agent", System.getProperty("http.agent"));
		return defaultHeaders;
	}
}
