package com.dansam.climate.volley.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dansam.climate.api.dto.common.ErrorDTO;
import com.dansam.climate.volley.provider.VolleyRequest.VolleyResponseCallback;

/**
 * 
 * @author samkirton
 */
public class ResponseErrorListenerImpl implements Response.ErrorListener {
	private String mUrl;
	private VolleyResponseCallback mVolleyResponseCallback;
	
	public ResponseErrorListenerImpl(String url, VolleyResponseCallback volleyResponseCallback) {
		mUrl = url;
		mVolleyResponseCallback = volleyResponseCallback;
	}
	
	@Override
	public void onErrorResponse(VolleyError volleyError) {
		ErrorDTO errorDTO = new ErrorDTO();
		
		if (volleyError instanceof TimeoutError) {
			errorDTO = new ErrorDTO();
			errorDTO.setMessage("The request took too long to complete, please ensure you have an internet connection available");	
		} else if (volleyError instanceof NoConnectionError) {
			errorDTO = new ErrorDTO();
			errorDTO.setMessage("memtrip could not process your request, please try again");
		} else if (volleyError.networkResponse != null) {
			String json = new String(volleyError.networkResponse.data);
			try {
				errorDTO = new ErrorDTO(new JSONObject(json));
			} catch (JSONException e) { }
		} else {
			errorDTO = new ErrorDTO();
			errorDTO.setMessage(volleyError.getMessage());	
		}
		
		mVolleyResponseCallback.onVolleyError(mUrl, errorDTO);
	}
}
