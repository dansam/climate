package com.dansam.climate.api.dto.base;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author samkirton
 */
public interface IDTO {
	public void fromJson(JSONObject jsonObject) throws JSONException;
	public JSONObject toJson() throws JSONException;
}
