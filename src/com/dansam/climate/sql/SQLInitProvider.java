package com.dansam.climate.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dansam.climate.helper.ResourceHelper;
import com.pingoverthere.R;

/**
 * A provider class that creates and updates a SQLite database schema
 * @author samkirton
 */
public class SQLInitProvider extends SQLiteOpenHelper {
	private Context mContext;
	
	private static final String DATABASE_NAME = "noah";
	private static final String DROP_QUERY = "DROP TABLE IF EXISTS ";
	private static final int VERSION = 1;
	
	/**
	 * Constructor
	 * @param	context	
	 */
	public SQLInitProvider(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		String eventCacheSchema = ResourceHelper.getFileContentsAsString(mContext, R.raw.event_cache);
		database.execSQL(eventCacheSchema);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL(DROP_QUERY + "EventCache");
		onCreate(database);
	}
}