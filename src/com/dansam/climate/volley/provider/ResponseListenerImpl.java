package com.dansam.climate.volley.provider;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.dansam.climate.api.dto.base.IDTO;
import com.dansam.climate.helper.ReflectionHelper;
import com.dansam.climate.volley.provider.VolleyRequest.VolleyResponseCallback;

/**
 * 
 * @author samkirton
 */
public class ResponseListenerImpl implements Response.Listener<JSONObject> {
	private String mUrl;
	private VolleyResponseCallback mVolleyResponseCallback;
	private Class<?> mClassDef;
	private Map<String,String> mHeaders;
	
	public void setHeaders(Map<String,String> headers) {
		mHeaders = headers;
	}
	
	public ResponseListenerImpl(String url, VolleyResponseCallback volleyResponseCallback, Class<?> classDef) {
		mUrl = url;
		mVolleyResponseCallback = volleyResponseCallback;
		mClassDef = classDef;
	}
	
	/**
	 * Create a new instance of a IDTO object based on the classRef provided.
	 * With the new object the fromJson() method is executed to create an 
	 * friendly object representation of the JSONObject.
	 * @param	jsonObject	The JSONObject to convert into a DTO
	 * @param	classRef	The class to create the new object instance from
	 * @return	The JSONObject as a friendly DTO 
	 */
	public IDTO buildResponseDTO(JSONObject jsonObject, Class<?> classRef) {
		IDTO responseDTO = null;
		
		if (jsonObject != null) {
			responseDTO = (IDTO)ReflectionHelper.newInstance(classRef);
			
			try {
				responseDTO.fromJson(jsonObject);
			} catch (JSONException e) { }
		}
		
		return responseDTO;
	}
	
	@Override
	public void onResponse(JSONObject jsonObject) {
		mVolleyResponseCallback.onVolleyResponse(mUrl,buildResponseDTO(jsonObject, mClassDef),mHeaders);
	}
}
