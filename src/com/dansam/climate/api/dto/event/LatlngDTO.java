// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

import org.json.JSONException;
import org.json.JSONObject;

import com.dansam.climate.api.dto.base.IDTO;

public final class LatlngDTO implements IDTO {
	private String latitude;
	private String longitude;

	public String getLatitude() { 
		return latitude;
	}

	public void setLatitude(String newVal) { 
		latitude = newVal;
	}

	public String getLongitude() { 
		return longitude;
	}

	public void setLongitude(String newVal) { 
		longitude = newVal;
	}

	public LatlngDTO() { }

	public LatlngDTO(JSONObject jsonObject) throws JSONException {
		fromJson(jsonObject);
	}

	@Override
	public void fromJson(JSONObject jsonObject) throws JSONException {
		this.latitude = jsonObject.has("latitude") ? jsonObject.getString("latitude") : null;
		this.longitude = jsonObject.has("longitude") ? jsonObject.getString("longitude") : null;
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();

		if (this.latitude instanceof String)
			jsonObject.put("latitude",this.latitude);
		if (this.longitude instanceof String)
			jsonObject.put("longitude",this.longitude);

		return jsonObject;
	}
}