package com.dansam.climate.asynctask.param;

import com.dansam.climate.asynctask.base.IParam;

/**
 * Provide the testVal String variable to an AsyncTask thread
 * @author samkirton
 */
public class TestParam implements IParam {
	private String mTestVal;
	
	public String getTestVal() {
		return mTestVal;
	}
	
	public void setTestVal(String newVal) {
		mTestVal = newVal;
	}
}
