// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dansam.climate.api.dto.base.IDTO;

public final class EventTypeListDTO implements IDTO {
	private String[] eventTypes;

	public String[] getEventTypes() { 
		return eventTypes;
	}

	public void setEventTypes(String[] newVal) { 
		eventTypes = newVal;
	}

	public EventTypeListDTO() { }

	public EventTypeListDTO(JSONObject jsonObject) throws JSONException {
		fromJson(jsonObject);
	}

	@Override
	public void fromJson(JSONObject jsonObject) throws JSONException {

		this.eventTypes = null;
		if (jsonObject.has("eventTypes")) {
			JSONArray jsonArrayEventTypes = new JSONArray(jsonObject.getString("eventTypes"));
			this.eventTypes = new String[jsonArrayEventTypes.length()];
			for (int i = 0; i < jsonArrayEventTypes.length(); i++) {
				String eventTypesToValue = jsonArrayEventTypes.getString(i);
				this.eventTypes[i] = eventTypesToValue;
			}
		}
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONArray jsonArrayEventTypes = new JSONArray();
		if (this.eventTypes instanceof String[]) {
			for (int i = 0; i < this.eventTypes.length; i++) {
				String jsonObjectEventTypes = this.eventTypes[i];
				jsonArrayEventTypes.put(jsonObjectEventTypes);
			}
			jsonObject.put("eventTypes",jsonArrayEventTypes);
		}

		return jsonObject;
	}
}