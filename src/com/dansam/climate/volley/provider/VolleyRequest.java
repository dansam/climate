package com.dansam.climate.volley.provider;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.dansam.climate.api.dto.base.IDTO;
import com.dansam.climate.api.dto.common.ErrorDTO;
import com.dansam.climate.helper.RESTHelper;

/**
 * A Volley JsonObjectRequest that provides a mechanism for sending the result
 * of the request to a callback.
 * @author samkirton
 */
public class VolleyRequest extends Request<JSONObject> {
	private JSONObject mRequestJSONObject;
	private HashMap<String, String> mHeaders;
	private ResponseListenerImpl mResponseListenerImpl;
	
	/**
	 * The response of a VolleyRequest, any Activity that runs a request
	 * must implement this interface to receive a response from Volley.
	 */
	public interface VolleyResponseCallback {
		public void onVolleyResponse(String url, IDTO dto, Map<String,String> headers);
		public void onVolleyError(String url, ErrorDTO errorDTO);
	}
	
	public void setHeaders(HashMap<String, String> headers) {
		mHeaders = headers;
	}
	
	/**
	 * Create a new HTTP request
	 * @param	method	The request HTTP method (GET, POST, PUT, DELETE)
	 * @param	url	The request URL
	 * @param	jsonRequest	The request JSONObject
	 * @param	responseClassDef	The expected class data type of the response
	 * @param	volleyResponseCallback	The callback to send the results to when the volley request has finished
	 */
	public VolleyRequest(int method, 
			String url, 
			JSONObject jsonRequest, 
			Class<?> responseClassDef, 
			VolleyResponseCallback volleyResponseCallback) {
		
		super(method, 
			url,
			new ResponseErrorListenerImpl(url, volleyResponseCallback)
		);
		
		mRequestJSONObject = jsonRequest;
		mResponseListenerImpl = new ResponseListenerImpl(url, volleyResponseCallback, responseClassDef);
	}
	
    @Override
    public HashMap<String, String> getHeaders() {
    	HashMap<String,String> defaultHeaders = RESTHelper.getDefaultHeadersList();
    	if (mHeaders == null) {
    		mHeaders = new HashMap<String, String>();
    	} 
    		
    	mHeaders.putAll(defaultHeaders);
    	
        return mHeaders;
    }
    
    @Override
    public byte[] getBody() {
    	if (mRequestJSONObject != null) {
    		return mRequestJSONObject.toString().getBytes();
    	} else {
    		return null;
    	}
    }
    
	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		mResponseListenerImpl.setHeaders(response.headers);
		
		if (response.statusCode == 204 || response.statusCode == 201) {
			return Response.success(null, null);
		} else if (response.statusCode == 200 || response.statusCode == 400) {
			try {
				String jsonString = new String(response.data,HttpHeaderParser.parseCharset(response.headers));
				return Response.success(new JSONObject(jsonString),HttpHeaderParser.parseCacheHeaders(response));
			} catch (UnsupportedEncodingException e) {
				return Response.error(new ParseError(e));
			} catch (JSONException je) {
				return Response.error(new ParseError(je));
			}
		} else if (response.statusCode == 403) {
			return Response.error(new VolleyError("Could not authenticate with memtrip, please logout before continuing"));
		} else if (response.statusCode == 500) {
			return Response.error(new VolleyError("Service unavailable"));
		} else {
			return Response.error(new VolleyError("Service unavailable"));
		}
	}
    
    @Override
    protected void deliverResponse(JSONObject response) {
    	mResponseListenerImpl.onResponse(response);
    }
}
