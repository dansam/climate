package com.dansam.climate.fragment;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dansam.climate.activity.base.BaseActivity;
import com.dansam.climate.api.dto.event.EventDTO;
import com.dansam.climate.helper.MapHelper;
import com.dansam.climate.location.GPSConnectivityException;
import com.dansam.climate.location.GPSLocationProvider;
import com.dansam.climate.location.GPSLocationProvider.LocationCallback;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.pingoverthere.R;

public class BoundaryMapFragment extends Fragment implements LocationCallback, OnMapClickListener, OnMarkerClickListener {
	private GoogleMap uiGoogleMap;
	private MapView uiMapView;
	private GPSLocationProvider mGPSLocationProvider;
	private LatLng mCurrentPosition;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_boundary_map, container, false);
		
		MapsInitializer.initialize(getActivity());
		
		uiMapView = (MapView) view.findViewById(R.id.fragment_boundary_map_MapView);
		uiMapView.onCreate(savedInstanceState);
	
		if (uiGoogleMap == null) {
			uiGoogleMap = uiMapView.getMap();
			uiGoogleMap.setOnMapClickListener(this);
			uiGoogleMap.setOnMarkerClickListener(this);
			
			mGPSLocationProvider = new GPSLocationProvider(getActivity().getBaseContext());
			mGPSLocationProvider.setLocationCallback(this);
			
			// attempt to get the last location from the GPSLocationProvider
			Location lastLocation = null;
			boolean noGPSAndNetworkConnectivity = false;
		
			try {
				lastLocation = mGPSLocationProvider.getLocation();
			} catch (GPSConnectivityException e) { }
			
			if (noGPSAndNetworkConnectivity || lastLocation == null) {
				// TODO network connectivity and GPS are not available, show error
			} else {
				mCurrentPosition = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
				MapHelper.moveCameraToPosition(uiGoogleMap, mCurrentPosition);
			}
		}
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		uiMapView.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		uiMapView.onPause();
	}
	
	/**
	 * Draw saved notifications on the map
	 */
	public void init(EventDTO[] eventDTOArray) {
		for (int i = 0; i < eventDTOArray.length; i++) {
			EventDTO eventDTO = eventDTOArray[i];
			LatLng latLng = new LatLng(
				Double.parseDouble(eventDTO.getLatlng().getLatitude()), 
				Double.parseDouble(eventDTO.getLatlng().getLongitude())
			);
			
			MapHelper.drawPoint(uiGoogleMap, latLng);
		}
	}
	
	/**
	 * Clear the google map of any markers and boundaries
	 */
	public void clear() {
		uiGoogleMap.clear();
	}

	/**
	 * Draw the notification boundary circle on the map at the provided position
	 * @param	position	The position to draw the boundary
	 */
	private void map_Click(LatLng position) {
		MapHelper.drawPoint(uiGoogleMap, position);
		Bundle bundle = new Bundle();
		bundle.putString(SaveEventFragment.EXTRA_LATITUDE, String.valueOf(position.latitude));
		bundle.putString(SaveEventFragment.EXTRA_LONGITUDE, String.valueOf(position.longitude));
		
		SaveEventFragment saveEventFragment = new SaveEventFragment();
		saveEventFragment.setArguments(bundle);
		((BaseActivity)getActivity()).showDialog(saveEventFragment);
	}
	
	/**
	 * @param	position	The position of the marker that has been clicked
	 */
	private void marker_Click(LatLng position) {
		// clicked
	}
	
	@Override
	public boolean onMarkerClick(Marker marker) {
		marker_Click(marker.getPosition());
		return true;
	}

	@Override
	public void onMapClick(LatLng position) {
		map_Click(position);
	}

	@Override
	public void onLocationRetrieved(Location location) {
		mCurrentPosition = new LatLng(location.getLatitude(), location.getLongitude());
		MapHelper.moveCameraToPosition(uiGoogleMap,mCurrentPosition);
	}

	@Override
	public void onProviderDisabled() {
		// TODO Auto-generated method stub
	}
}