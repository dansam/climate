package com.dansam.climate.volley.provider;

import java.util.HashMap;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Request;
import com.dansam.climate.ClimateApplication;
import com.dansam.climate.api.dto.event.EventDTO;
import com.dansam.climate.api.dto.event.EventListDTO;
import com.dansam.climate.api.dto.event.EventTypeListDTO;
import com.dansam.climate.volley.provider.VolleyRequest.VolleyResponseCallback;
import com.pingoverthere.R;

/**
 * File generated on: 27/04/2014 11:53:49
 */
public class VolleyRequestProvider {
	private String mRestEndPoint;
	private VolleyResponseCallback mVolleyResponseCallback;

	public static final String EVENT = "/event/";
	public static final String EVENT_TYPES = "/event/types/";

	public void setVolleyResponseCallback(VolleyResponseCallback volleyResponseCallback) {
		mVolleyResponseCallback = volleyResponseCallback;
	}

	public VolleyRequestProvider(Context context) {
		mRestEndPoint = context.getResources().getString(R.string.sys_rest_endpoint);
	}

	// URL: /event/
	public VolleyRequest getEvent(long longitude, long latitude, long radius, HashMap<String, String> headers) {
		final int httpMethod = Request.Method.GET;

		int parameterCount = 0;
		String parameters = "";

		if (longitude != -1) {
			String leadChar = (parameterCount > 0) ? "&" : "?";
			parameters = parameters + leadChar + "longitude=" + String.valueOf(longitude);
			parameterCount++;
		}

		if (latitude != -1) {
			String leadChar = (parameterCount > 0) ? "&" : "?";
			parameters = parameters + leadChar + "latitude=" + String.valueOf(latitude);
			parameterCount++;
		}

		if (radius != -1) {
			String leadChar = (parameterCount > 0) ? "&" : "?";
			parameters = parameters + leadChar + "radius=" + String.valueOf(radius);
			parameterCount++;
		}

		VolleyRequest request = null;
		request = new VolleyRequest(
			httpMethod,
			mRestEndPoint + "/event/" + parameters,
			null,
			EventListDTO.class,
			mVolleyResponseCallback
		);

		request.setHeaders(headers);

		ClimateApplication.getInstance().getRequestQueue().add(request);

		return request;
	}

	// URL: /event/
	public VolleyRequest saveEvent(EventDTO dto, HashMap<String, String> headers) {
		final int httpMethod = Request.Method.POST;

		int parameterCount = 0;
		String parameters = "";

		VolleyRequest request = null;
		try {
			request = new VolleyRequest(
				httpMethod,
				mRestEndPoint + "/event/" + parameters,
				dto.toJson(),
				null,
				mVolleyResponseCallback
			);

			request.setHeaders(headers);

			ClimateApplication.getInstance().getRequestQueue().add(request);
		} catch (JSONException e) {
			throw new IllegalStateException("The JSON object is malformed");
		}
		return request;
	}

	// URL: /event/types/
	public VolleyRequest getEvent(HashMap<String, String> headers) {
		final int httpMethod = Request.Method.GET;

		int parameterCount = 0;
		String parameters = "";

		VolleyRequest request = null;
		request = new VolleyRequest(
			httpMethod,
			mRestEndPoint + "/event/types/" + parameters,
			null,
			EventTypeListDTO.class,
			mVolleyResponseCallback
		);

		request.setHeaders(headers);

		ClimateApplication.getInstance().getRequestQueue().add(request);

		return request;
	}
}