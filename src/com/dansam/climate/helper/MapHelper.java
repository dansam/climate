package com.dansam.climate.helper;

import android.content.Context;
import android.location.Location;

import com.dansam.climate.struct.MapDrawStruct;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Helper classes for the google maps
 * @author samkirton
 */
public class MapHelper {
	/**
	 * @param	circle	The circle where the marker residers
	 * @param	position	Check whether this position is within the circle 	
	 * @param	context	The application context
	 * @return	Whether the position is within the provided circle
	 */
	public static boolean distFrom2LocationsInMeter(Circle circle, LatLng position, Context context) {
		boolean isInRadius = false;
		float[] distance = new float[2];
		double circleRadius = circle.getRadius();
		
		Location.distanceBetween(
			position.latitude, 
			position.longitude,
			circle.getCenter().latitude, 
			circle.getCenter().longitude, 
			distance
		);

		if (distance[0] < circleRadius) {
			isInRadius = true;
		}
		
		return isInRadius;
	}
	
	/**
	 * Move the google map to the specified position
	 * @param	googleMap	The google map that the camera is being moved for
	 * @param	position	The position that the map should be moved to
	 */
	public static void moveCameraToPosition(GoogleMap googleMap, LatLng position) {
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
	}
	
	/**
	 * Draw a point on the map at the desired position and specified radius
	 * @param	googleMap	The googleMap object to draw the circle on	
	 * @param	point	The LatLng to draw the object
	 * @param	radius	The radius of the circle
	 * @param	color	The color of the circle
	 * @return	A circle object based on what was drawn
	 */
	public static MapDrawStruct drawPoint(GoogleMap googleMap, LatLng point) {
		Marker marker = googleMap.addMarker(new MarkerOptions().position(point));
		
		MapDrawStruct mapDrawStruct = new MapDrawStruct();
		mapDrawStruct.setMarker(marker);
		//TOOD: Set an icon
		
		return mapDrawStruct;
	}
}