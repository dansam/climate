// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

public class StatusENUM {
	public static final String ACTIVE = "ACTIVE";
	public static final String INACTIVE = "INACTIVE";

	private String value;

	public StatusENUM(String value) {
		if (value.equals(ACTIVE) || value.equals(INACTIVE)) {
			this.value = value;
		} else {
			throw new IllegalArgumentException("Invalid enum value");
		}
	}

	public String getValue() {
		return value;
	}
}