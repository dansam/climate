// File generated on: 27/04/2014 11:53:49
package com.dansam.climate.api.dto.event;

import org.json.JSONException;
import org.json.JSONObject;

import com.dansam.climate.api.dto.base.IDTO;

public final class EventDTO implements IDTO {
	private LatlngDTO latlng;
	private String mobileNumber;
	private String postCode;
	private String message;
	private long timestamp;
	private EventTypeENUM eventType;
	private FloodStatusENUM floodStatus;
	private StatusENUM status;

	public LatlngDTO getLatlng() { 
		return latlng;
	}

	public void setLatlng(LatlngDTO newVal) { 
		latlng = newVal;
	}

	public String getMobileNumber() { 
		return mobileNumber;
	}

	public void setMobileNumber(String newVal) { 
		mobileNumber = newVal;
	}

	public String getPostCode() { 
		return postCode;
	}

	public void setPostCode(String newVal) { 
		postCode = newVal;
	}

	public String getMessage() { 
		return message;
	}

	public void setMessage(String newVal) { 
		message = newVal;
	}

	public long getTimestamp() { 
		return timestamp;
	}

	public void setTimestamp(long newVal) { 
		timestamp = newVal;
	}

	public EventTypeENUM getEventType() { 
		return eventType;
	}

	public void setEventType(EventTypeENUM newVal) { 
		eventType = newVal;
	}

	public FloodStatusENUM getFloodStatus() { 
		return floodStatus;
	}

	public void setFloodStatus(FloodStatusENUM newVal) { 
		floodStatus = newVal;
	}

	public StatusENUM getStatus() { 
		return status;
	}

	public void setStatus(StatusENUM newVal) { 
		status = newVal;
	}

	public EventDTO() { }

	public EventDTO(JSONObject jsonObject) throws JSONException {
		fromJson(jsonObject);
	}

	@Override
	public void fromJson(JSONObject jsonObject) throws JSONException {
		this.mobileNumber = jsonObject.has("mobileNumber") ? jsonObject.getString("mobileNumber") : null;
		this.postCode = jsonObject.has("postCode") ? jsonObject.getString("postCode") : null;
		this.message = jsonObject.has("message") ? jsonObject.getString("message") : null;
		this.timestamp = jsonObject.has("timestamp") ? jsonObject.getLong("timestamp") : -1;

		if (jsonObject.has("latlng")) {
			JSONObject latlngJSONObject = jsonObject.getJSONObject("latlng");
			LatlngDTO latlngDTO = new LatlngDTO(latlngJSONObject);
			this.latlng = latlngDTO;
		}

		this.eventType = null;
		if (jsonObject.has("eventType")) {
			this.eventType = new EventTypeENUM(jsonObject.getString("eventType"));
		}
		this.floodStatus = null;
		if (jsonObject.has("floodStatus")) {
			this.floodStatus = new FloodStatusENUM(jsonObject.getString("floodStatus"));
		}
		this.status = null;
		if (jsonObject.has("status")) {
			this.status = new StatusENUM(jsonObject.getString("status"));
		}
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();

		if (this.mobileNumber instanceof String)
			jsonObject.put("mobileNumber",this.mobileNumber);
		if (this.postCode instanceof String)
			jsonObject.put("postCode",this.postCode);
		if (this.message instanceof String)
			jsonObject.put("message",this.message);
		jsonObject.put("timestamp",this.timestamp);

		if (this.latlng instanceof LatlngDTO) {
			JSONObject latlngJSONObject = this.latlng.toJson();
			jsonObject.put("latlng",latlngJSONObject);
		}

		if (this.eventType instanceof EventTypeENUM)
			jsonObject.put("eventType",this.eventType.getValue());
		if (this.floodStatus instanceof FloodStatusENUM)
			jsonObject.put("floodStatus",this.floodStatus.getValue());
		if (this.status instanceof StatusENUM)
			jsonObject.put("status",this.status.getValue());

		return jsonObject;
	}
}