package com.dansam.climate.fragment;

import com.pingoverthere.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * The left navigation drawer fragment, the class should be renamed to something 
 * more descriptive once we know what it will be used for!
 * @author samkirton
 */
public class NavigationDrawerLeftFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_navigation_drawer_left, container, false);
		return view;
	}
}
