// File generated on: 26/04/2014 12:33:21
package com.dansam.climate.api.dto.common;

import org.json.JSONException;
import org.json.JSONObject;

import com.dansam.climate.api.dto.base.IDTO;

public final class ErrorDTO implements IDTO {
	private String errorCode;
	private String message;

	public String getErrorCode() { 
		return errorCode;
	}

	public void setErrorCode(String newVal) { 
		errorCode = newVal;
	}

	public String getMessage() { 
		return message;
	}

	public void setMessage(String newVal) { 
		message = newVal;
	}

	public ErrorDTO() { }

	public ErrorDTO(JSONObject jsonObject) throws JSONException {
		fromJson(jsonObject);
	}

	@Override
	public void fromJson(JSONObject jsonObject) throws JSONException {
		this.errorCode = jsonObject.has("errorCode") ? jsonObject.getString("errorCode") : null;
		this.message = jsonObject.has("message") ? jsonObject.getString("message") : null;
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();

		if (this.errorCode instanceof String)
			jsonObject.put("errorCode",this.errorCode);
		if (this.message instanceof String)
			jsonObject.put("message",this.message);

		return jsonObject;
	}
}