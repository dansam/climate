package com.dansam.climate.asynctask.response;

import com.dansam.climate.asynctask.base.BaseResponse;

/**
 * The response of the AsyncTask
 * @author samkirton
 */
public class TestResponse extends BaseResponse {
	private String mResponseVal;
	
	public String getResponseVal() {
		return mResponseVal;	
	}
	
	public void setResponseVal(String newVal) {
		mResponseVal = newVal;
	}
}
